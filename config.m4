dnl config.m4 for extension csv

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary.

PHP_ARG_ENABLE([csv],
  [whether to enable csv support],
  [AS_HELP_STRING([--enable-csv],
    [Enable csv support])],
  [no])

if test "$PHP_CSV" != "no"; then
  dnl In case of no dependencies
  AC_DEFINE(HAVE_CSV, 1, [ Have csv support ])

  PHP_NEW_EXTENSION(csv, csv.c, $ext_shared)
fi
