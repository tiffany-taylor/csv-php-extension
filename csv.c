/* csv extension for PHP */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "php.h"
#include "ext/standard/info.h"
#include "ext/standard/php_string.h"
#include "php_csv.h"
#include "csv_arginfo.h"

#include <stdbool.h>
#include "zend_smart_str.h"

/* {{{ PHP_RINIT_FUNCTION
 */
PHP_RINIT_FUNCTION(csv)
{
#if defined(ZTS) && defined(COMPILE_DL_CSV)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(csv)
{
	php_info_print_table_start();
		php_info_print_table_header(2, "CSV support", "enabled");
		php_info_print_table_header(2, "Version", PHP_CSV_VERSION);
		php_info_print_table_header(2, "Author", "George Peter Banyard");
		php_info_print_table_header(2, "Bug reports", "https://gitlab.com/Girgias/csv-php-extension/issues");
	php_info_print_table_end();
}
/* }}} */

/**
 * Follows RFC4180 https://tools.ietf.org/html/rfc4180
 * The terminology can be slightly confusing here as what PHP considers the 'enclosure' is what is used
 * to escape a field in the RFC.
 * 
 * This only formats ONE line of a CSV file.
 */
static zend_string* hashtable_to_rfc4180_string(HashTable *fields, zend_string *delimiter, zend_string *enclosure)
{
	int nb_fields;
	int fields_iterated = 0;
	zval *tmp_field_zval;
	smart_str csv_line = {0};
	zend_string *return_value;

	nb_fields = zend_hash_num_elements(fields);
	ZEND_HASH_FOREACH_VAL(fields, tmp_field_zval) {
		zend_string *tmp_field_str;
		zend_string *field_str = zval_get_tmp_string(tmp_field_zval, &tmp_field_str);
		int escape_field = 0;
		int enclosure_within_filed = 0;

		/*
		 * A field must be escaped (enclosed) if it contains the delimiter OR a Carriage Return (\r) 
		 * OR a Line Feed (\n) OR the field escape sequence (i.e. enclosure parameter)
		 */
		if (
			memchr(ZSTR_VAL(field_str), '\n', ZSTR_LEN(field_str))
			|| memchr(ZSTR_VAL(field_str), '\r', ZSTR_LEN(field_str))
			|| php_memnstr(ZSTR_VAL(field_str), ZSTR_VAL(delimiter), ZSTR_LEN(delimiter),
				ZSTR_VAL(field_str) + ZSTR_LEN(field_str))
		) {
			escape_field = 1;
		}
		/* If the field escape sequence (i.e. enclosure parameter) is within the field it needs to be
		 * duplicated within the field. */
		if (php_memnstr(ZSTR_VAL(field_str), ZSTR_VAL(enclosure), ZSTR_LEN(enclosure),
			ZSTR_VAL(field_str) + ZSTR_LEN(field_str))) {
			escape_field = 1;
			enclosure_within_filed = 1;
		}

		if (escape_field) {
			smart_str_append(&csv_line, enclosure);

			if (enclosure_within_filed) {
				/* Create replace string (twice the field escape sequence) */
				smart_str escaped_enclosure = {0};
				smart_str_append(&escaped_enclosure, enclosure);
				smart_str_append(&escaped_enclosure, enclosure);
				smart_str_0(&escaped_enclosure);

				zend_string *replace = php_str_to_str(ZSTR_VAL(field_str), ZSTR_LEN(field_str), ZSTR_VAL(enclosure),
						ZSTR_LEN(enclosure), ZSTR_VAL(escaped_enclosure.s), ZSTR_LEN(escaped_enclosure.s));
				
				smart_str_append(
					&csv_line,
					replace
				);

				smart_str_free(&escaped_enclosure);
				zend_string_release(replace);
			} else {
				smart_str_append(&csv_line, field_str);
			}

			smart_str_append(&csv_line, enclosure);
		} else {
			smart_str_append(&csv_line, field_str);
		}

		/* Only add the delimiter in between fields on the same line. */
		if (++fields_iterated != nb_fields) {
			smart_str_append(&csv_line, delimiter);
		}

		/* Clear temporary variable */
		zend_tmp_string_release(tmp_field_str);
	} ZEND_HASH_FOREACH_END();
	/* Add a CRLF to indicate the end of the line. */
	smart_str_appends(&csv_line, "\r\n");

	smart_str_0(&csv_line);

	return_value = zend_string_init(ZSTR_VAL(csv_line.s), ZSTR_LEN(csv_line.s), 0);

	smart_str_free(&csv_line);

	return return_value;
}

static HashTable* rfc4180_string_to_hashtable(zend_string *csv_line, zend_string *delimiter, zend_string *enclosure)
{
	HashTable *return_value = zend_new_array(8);

	bool in_escaped_field = false;

	char *c_csv_line = ZSTR_VAL(csv_line);

	smart_str field_value = {0};
	size_t field_value_length = 0;

	/* Main loop to "tokenize" the csv_line */
	for (size_t index = 0; index < ZSTR_LEN(csv_line); ++index) {
		/* Check for field escape sequence (i.e. enclosure) */
		if (php_memnstr(c_csv_line, ZSTR_VAL(enclosure), ZSTR_LEN(enclosure), c_csv_line + ZSTR_LEN(enclosure))) {
			c_csv_line += ZSTR_LEN(enclosure);

			if (!in_escaped_field) {
				in_escaped_field = true;
				continue;
			}

			/* In an escaped field */
			if (php_memnstr(c_csv_line, ZSTR_VAL(enclosure), ZSTR_LEN(enclosure), c_csv_line + ZSTR_LEN(enclosure))) {
				in_escaped_field = true;
				goto consume;
			}

			in_escaped_field = false;
			continue;
		}

		/* Check for delimiter if not in an escaped field */
		if (
			!in_escaped_field
			&& php_memnstr(
				c_csv_line,
				ZSTR_VAL(delimiter),
				ZSTR_LEN(delimiter),
				c_csv_line + ZSTR_LEN(delimiter)
			)
		) {
			c_csv_line += ZSTR_LEN(delimiter);

			/* Add nul terminating byte */
			smart_str_0(&field_value);
			zval tmp;
			ZVAL_STRINGL(&tmp, ZSTR_VAL(field_value.s), field_value_length);
			zend_hash_next_index_insert(return_value, &tmp);

			smart_str_free(&field_value);

			field_value_length = 0;
			continue;
		}
		
		/* Check for End Of Line sequence (Carriage Return followed by New Line) when not in an escaped field */
		if (!in_escaped_field && c_csv_line[0] == '\r' && c_csv_line[1] == '\n') {
			goto eol;
		}

		consume:

		smart_str_appendc(&field_value, c_csv_line[0]);
		field_value_length++;
		c_csv_line++;
	}
	
	eol:
	/* Add nul terminating byte */
	smart_str_0(&field_value);
	zval tmp;
	ZVAL_STRINGL(&tmp, ZSTR_VAL(field_value.s), field_value_length);
	zend_hash_next_index_insert(return_value, &tmp);

	smart_str_free(&field_value);
	field_value_length = 0;
	c_csv_line += 2;
	
	return return_value;
}

PHP_FUNCTION(csv_array_to_row)
{
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	HashTable *fields;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "h|SS", &fields, &delimiter, &enclosure) == FAILURE) {
		return;
		/* Needs to be RETURN_THROWS(); in PHP 8.0. */
	}

	if (delimiter) {
		/* Make sure that there is at least one character in string */
		if (ZSTR_LEN(delimiter) < 1) {
			/*
			 * Needs to be ValueError and RETURN_THROWS(); in PHP 8.0.
			zend_value_error("Delimiter cannot be empty");
			RETURN_THROWS();
			*/
			zend_throw_error(NULL, "Delimiter cannot be empty");
			return;
		}
		if (0 == zend_binary_strcmp(ZSTR_VAL(delimiter), ZSTR_LEN(delimiter), "\r\n", strlen("\r\n"))) {
			/*
			 * Needs to be ValueError and RETURN_THROWS(); in PHP 8.0.
			zend_value_error("Delimiter cannot be the EOL sequence Carriage Return New Line (\\r\\n)");
			RETURN_THROWS();
			*/
			zend_throw_error(NULL, "Delimiter cannot be the EOL sequence Carriage Return New Line (\\r\\n)");
			return;
		}
	} else {
		delimiter = zend_string_init(",", strlen(","), 0);
	}

	if (enclosure) {
		if (ZSTR_LEN(enclosure) < 1) {
			/*
			 * Needs to be ValueError and RETURN_THROWS(); in PHP 8.0.
			zend_value_error("Enclosure cannot be empty");
			RETURN_THROWS();
			*/
			zend_throw_error(NULL, "Enclosure cannot be empty");
			return;
		}
		if (0 == zend_binary_strcmp(ZSTR_VAL(enclosure), ZSTR_LEN(enclosure), "\r\n", strlen("\r\n"))) {
			/*
			 * Needs to be ValueError and RETURN_THROWS(); in PHP 8.0.
			zend_value_error("Enclosure cannot be the EOL sequence Carriage Return New Line (\\r\\n)");
			RETURN_THROWS();
			*/
			zend_throw_error(NULL, "Enclosure cannot be the EOL sequence Carriage Return New Line (\\r\\n)");
			zend_string_release(delimiter);
			return;
		}
	} else {
		enclosure = zend_string_init("\"", strlen("\""), 0);
	}

	if (0 == zend_binary_strcmp(ZSTR_VAL(delimiter), ZSTR_LEN(delimiter), ZSTR_VAL(enclosure), ZSTR_LEN(enclosure))) {
		/*
		 * Needs to be ValueError and RETURN_THROWS(); in PHP 8.0.
		zend_value_error("Delimiter and enclosure cannot be identical");
		RETURN_THROWS();
		*/
		zend_throw_error(NULL, "Delimiter and enclosure cannot be identical");
		zend_string_release(delimiter);
		zend_string_release(enclosure);
		return;
	}

	RETVAL_STR(hashtable_to_rfc4180_string(fields, delimiter, enclosure));
	zend_string_release(delimiter);
	zend_string_release(enclosure);
}

PHP_FUNCTION(csv_row_to_array)
{
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	zend_string *csv_line;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "S|SS", &csv_line, &delimiter, &enclosure) == FAILURE) {
		return;
		/* Needs to be RETURN_THROWS(); in PHP 8.0. */
	}

	if (delimiter) {
		/* Make sure that there is at least one character in string */
		if (ZSTR_LEN(delimiter) < 1) {
			/*
			 * Needs to be ValueError and RETURN_THROWS(); in PHP 8.0.
			zend_value_error("Delimiter cannot be empty");
			RETURN_THROWS();
			*/
			zend_throw_error(NULL, "Delimiter cannot be empty");
			return;
		}
		if (0 == zend_binary_strcmp(ZSTR_VAL(delimiter), ZSTR_LEN(delimiter), "\r\n", strlen("\r\n"))) {
			/*
			 * Needs to be ValueError and RETURN_THROWS(); in PHP 8.0.
			zend_value_error("Delimiter cannot be the EOL sequence Carriage Return New Line (\\r\\n)");
			RETURN_THROWS();
			*/
			zend_throw_error(NULL, "Delimiter cannot be the EOL sequence Carriage Return New Line (\\r\\n)");
			return;
		}
	} else {
		delimiter = zend_string_init(",", strlen(","), 0);
	}

	if (enclosure) {
		if (ZSTR_LEN(enclosure) < 1) {
			/*
			 * Needs to be ValueError and RETURN_THROWS(); in PHP 8.0.
			zend_value_error("Enclosure cannot be empty");
			RETURN_THROWS();
			*/
			zend_throw_error(NULL, "Enclosure cannot be empty");
			return;
		}
		if (0 == zend_binary_strcmp(ZSTR_VAL(enclosure), ZSTR_LEN(enclosure), "\r\n", strlen("\r\n"))) {
			/*
			 * Needs to be ValueError and RETURN_THROWS(); in PHP 8.0.
			zend_value_error("Enclosure cannot be the EOL sequence Carriage Return New Line (\\r\\n)");
			RETURN_THROWS();
			*/
			zend_throw_error(NULL, "Enclosure cannot be the EOL sequence Carriage Return New Line (\\r\\n)");
			zend_string_release(delimiter);
			return;
		}
	} else {
		enclosure = zend_string_init("\"", strlen("\""), 0);
	}

	/* Ensure delimiter and enclosure are different */
	if (0 == zend_binary_strcmp(ZSTR_VAL(delimiter), ZSTR_LEN(delimiter), ZSTR_VAL(enclosure), ZSTR_LEN(enclosure))) {
		/*
		 * Needs to be ValueError and RETURN_THROWS(); in PHP 8.0.
		zend_value_error("Delimiter and enclosure cannot be identical");
		RETURN_THROWS();
		*/
		zend_throw_error(NULL, "Delimiter and enclosure cannot be identical");
		zend_string_release(delimiter);
		zend_string_release(enclosure);
		return;
	}
	RETVAL_ARR(rfc4180_string_to_hashtable(csv_line, delimiter, enclosure));
	zend_string_release(delimiter);
	zend_string_release(enclosure);
}

/* {{{ csv_functions[]
 */
static const zend_function_entry csv_functions[] = {
	PHP_FE(csv_array_to_row,	arginfo_csv_array_to_row)
	PHP_FE(csv_row_to_array,	arginfo_csv_row_to_array)
	PHP_FE_END
};
/* }}} */

/* {{{ csv_module_entry
 */
zend_module_entry csv_module_entry = {
	STANDARD_MODULE_HEADER,
	"csv",					/* Extension name */
	csv_functions,			/* zend_function_entry */
	NULL,					/* PHP_MINIT - Module initialization */
	NULL,					/* PHP_MSHUTDOWN - Module shutdown */
	PHP_RINIT(csv),			/* PHP_RINIT - Request initialization */
	NULL,					/* PHP_RSHUTDOWN - Request shutdown */
	PHP_MINFO(csv),			/* PHP_MINFO - Module info */
	PHP_CSV_VERSION,		/* Version */
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_CSV
# ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
# endif
ZEND_GET_MODULE(csv)
#endif