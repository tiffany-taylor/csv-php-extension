# PHP CSV Extension

A small PHP extension to add/improve the handling of CSV strings which follows
RFC 4180 https://tools.ietf.org/html/rfc4180

## Functionality
There are currently two functions which are inspired from ``fputcsv()`` and
``fgetcsv()`` namely ``csv_array_to_row()`` and ``csv_row_to_array()``
respectively.

These functions are binary safe (i.e. accepts nul bytes) and accept multi-bytes
strings as the delimiter and enclosure.

One limitation is that non ASCII compatible character encodings (e.g. UTF-16)
are not supported. However, this is already the case with the current CSV
functions, thus no support for those is currently planned.

```php
function csv_array_to_row(array $fields, string $delimiter = ',', string $enclosure = '"'): string
```
Formats a CSV line with the given delimiter and enclosure (the field escape
sequence) as per RFC4180, i.e. terminated with a CRLF.


```php
function csv_row_to_array(string $csvLine,  string $delimiter = ',', string $enclosure = '"'): array
```
Returns an array containing the fields provided from the ``$csvLine`` string.
This string should follow RFC4180.


## Future scope

The plan is to propose an RFC and add this extension to the core of PHP with
a timeline on deprecating the non-compliant ``str_getcsv()``, ``fputcsv()``,
and ``fgetcsv()`` functions.

Another reason to deprecate ``fputcsv()``, and ``fgetcsv()`` is that they have
multiple responsibilities.

## Bug reports

To report a bug, please provide a reproducible test script, in preference as a
PHPT test file and open an issue on Gitlab at
https://gitlab.com/Girgias/csv-php-extension/issues

For security issues please email me directly at girgias@php.net
