====== PHP RFC: RFC 4180 compliant CSV Extension ======
  * Date: 2020-mm-dd
  * Author: George Peter Banyard <girgias@php.net>
  * Target Version: PHP 8.0
  * Status: Under Discussion
  * Implementation: https://gitlab.com/Girgias/csv-php-extension

===== Introduction =====

PHP's current tool set to deal with CSV strings/files is suboptimal.

First of all there is no inverse function for <php>str_getcsv()</php> and the
current best approximation is to <php>fputcsv()</php> into a php://memory
resource and then read this back.

Secondly, they don't follow RFC 4180, and use a proprietary escape mechanism
which is incompatible with every software out there. This got //slightly//
better in PHP 7.4.0, as it is now possible to disable it.

===== Proposal =====

Include a new extension into PHP which handles the conversion between
arrays and strings for CSV rows following RFC 4180. [1]

===== Proposed PHP Version(s) =====

PHP 8.0.

===== Open Issues =====

Should it be possible to supply a custom delimiter?
Should it be possible to supply a custom enclosure?
Should the extension provide a parser which acts on files directly?

===== Future Scope =====

Deprecation of <php>str_getcsv()</php>, <php>fputcsv()</php>, and
<php>fgetcsv()</php> functions.

===== Proposed Voting Choices =====

Simple yes/no vote for the addition of the CSV Extension.

===== Implementation =====

An implementation of this extension can be found on my personal
[[https://gitlab.com/Girgias/csv-php-extension|GitLab]]

===== References =====

[1] RFC 4180: [[https://tools.ietf.org/html/rfc4180\|https://tools.ietf.org/html/rfc4180\]]\\

RFC Announcement:
