--TEST--
Error conditions for csv_row_to_array()
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$string = "";

// Empty delimiter or enclosure
try {
    var_dump(csv_row_to_array($string, ''));
} catch (\Error $e) {
    echo $e->getMessage() . \PHP_EOL;
}

try {
    var_dump(csv_row_to_array($string, '', '"'));
} catch (\Error $e) {
    echo $e->getMessage() . \PHP_EOL;
}

try {
    var_dump(csv_row_to_array($string, ',', ''));
} catch (\Error $e) {
    echo $e->getMessage() . \PHP_EOL;
}

// Same delimiter and enclosure
try {
    var_dump(csv_row_to_array($string, ',', ','));
} catch (\Error $e) {
    echo $e->getMessage() . \PHP_EOL;
}

// Default value for the enclosure is "
try {
    var_dump(csv_row_to_array($string, '"'));
} catch (\Error $e) {
    echo $e->getMessage() . \PHP_EOL;
}

// Delimiter or enclosure using the \r\n sequence
try {
    var_dump(csv_row_to_array($string, "\r\n"));
} catch (\Error $e) {
    echo $e->getMessage() . \PHP_EOL;
}

// Default value for the enclosure is "
try {
    var_dump(csv_row_to_array($string, ',', "\r\n"));
} catch (\Error $e) {
    echo $e->getMessage() . \PHP_EOL;
}

?>
--EXPECT--
Delimiter cannot be empty
Delimiter cannot be empty
Enclosure cannot be empty
Delimiter and enclosure cannot be identical
Delimiter and enclosure cannot be identical
Delimiter cannot be the EOL sequence Carriage Return New Line (\r\n)
Enclosure cannot be the EOL sequence Carriage Return New Line (\r\n)
